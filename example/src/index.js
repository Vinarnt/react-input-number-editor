import React, { useState } from 'react';
import { render } from 'react-dom';

import './style.css';
import LogContainer from './LogContainer';
import InputNumberEditor from 'react-input-number-editor';
import StatefulInputNumberEditor from './StatefulInputNumberEditor';

function Demo() {
    const [logs, setLogs] = useState([]);
    const [externalChangeInputValue, setExternalChangeInputValue] = useState(0);

    function handleInputChange(value) {
        setLogs([...logs, 'Value changed to ' + value]);
    }

    function handleExternalChangeInputClick() {
        setExternalChangeInputValue(externalChangeInputValue + 1);
    }

    function handleExternalChangeInputChange(value) {
        setExternalChangeInputValue(value);
    }

    return (
        <>
            <h1>react-input-number-editor Demo</h1>
            <h2>Basic usage</h2>
            <InputNumberEditor value={0} />
            <h2>Lower slide modifier</h2>
            <InputNumberEditor value={0} slideModifier={0.1} />
            <h2>Using one way range (Minimum: 0)</h2>
            <InputNumberEditor value={0} min={0} />
            <h2>Using two ways range (Minimum: 0, Maximum: 100)</h2>
            <InputNumberEditor value={0} min={0} max={100} />
            <h2>Using precision</h2>
            <InputNumberEditor value={0} precision={1} />
            <h2>Using step and step modifier</h2>
            <InputNumberEditor value={0} step={10} stepModifier={5} />
            <h2>Listen to changes</h2>
            <StatefulInputNumberEditor
                value={0}
                precision={1}
                min={0}
                onChange={handleInputChange}
            />
            <LogContainer logs={logs} />
            <h2>External value change</h2>
            <InputNumberEditor
                value={externalChangeInputValue}
                onChange={handleExternalChangeInputChange}
            />
            <button onClick={handleExternalChangeInputClick}>Add</button>

          <h2>Use custom css classes</h2>
          <InputNumberEditor className={"bordered blue"} />
          <h2>Read only input</h2>
          <InputNumberEditor readOnly />
        </>
    );
}

render(<Demo />, document.querySelector('#root'));
